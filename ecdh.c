
#define BN_DEBUG

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <openssl/dh.h>
#include <openssl/asn1.h>
#include <openssl/sha.h>
#include <openssl/bn.h>

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>

void hexprint(unsigned char *printBuf, int len)
{
  int i;
  for(i = 0; i < len; i++)
    {
      printf("%x ", printBuf[i]);
    }
  printf("\n");
}

#include <openssl/evp.h>
#include <openssl/ec.h>

#if 0
static const int KDF1_SHA1_len = 20;
static void *KDF1_SHA1(const void *in, size_t inlen, void *out, size_t *outlen)
	{
#ifndef OPENSSL_NO_SHA
	if (*outlen < SHA_DIGEST_LENGTH)
		return NULL;
	else
		*outlen = SHA_DIGEST_LENGTH;
	return SHA1(in, inlen, out);
#else
	return NULL;
#endif	/* OPENSSL_NO_SHA */
	}
#endif

void handleErrors(void) {
	exit(1);
}

unsigned char *ecdh(size_t *secret_len)
{
	BIO *stdout_bio;
	EVP_PKEY_CTX *pctx, *kctx, *peer_kctx;
	EVP_PKEY_CTX *ctx=NULL;
	unsigned char *secret=NULL;
	EVP_PKEY *pkey = NULL, *peer_key = NULL, *params = NULL;
	/* NB: assumes pkey, peerkey have been already set up */

	/* Create the context for parameter generation */
	if(NULL == (pctx = EVP_PKEY_CTX_new_id(EVP_PKEY_EC, NULL))) handleErrors();

	/* Initialise the parameter generation */
	if(1 != EVP_PKEY_paramgen_init(pctx)) handleErrors();

	/* We're going to use the ANSI X9.62 Prime 256v1 curve */
	if(1 != EVP_PKEY_CTX_set_ec_paramgen_curve_nid(pctx, NID_X9_62_prime256v1)) handleErrors();

	/* Create the parameter object params */
	if (!EVP_PKEY_paramgen(pctx, &params)) handleErrors();

	{
	
		/* Create the context for the key generation */
		if(NULL == (kctx = EVP_PKEY_CTX_new(params, NULL))) handleErrors();
		
		/* Generate the key */
		if(1 != EVP_PKEY_keygen_init(kctx)) handleErrors();
		if (1 != EVP_PKEY_keygen(kctx, &pkey)) handleErrors();
		
		stdout_bio = BIO_new_fp (stdout, BIO_NOCLOSE);
		
		printf("================= Parameter ================= \n");
		EVP_PKEY_print_params(stdout_bio, params,
				      5, NULL);
		
		printf("================= Public ================= \n");
		EVP_PKEY_print_public(stdout_bio, pkey,
				      5, NULL);
	
		printf("================= Private ================= \n");
		EVP_PKEY_print_private(stdout_bio, pkey,
				       5, NULL);
		
	}

	{
		
		/* Create the context for the key generation */
		if(NULL == (peer_kctx = EVP_PKEY_CTX_new(params, NULL))) handleErrors();
		
		/* Generate the key */
		if(1 != EVP_PKEY_keygen_init(peer_kctx)) handleErrors();
		if (1 != EVP_PKEY_keygen(peer_kctx, &peer_key)) handleErrors();
		
		stdout_bio = BIO_new_fp (stdout, BIO_NOCLOSE);
		
		printf("================= Parameter ================= \n");
		EVP_PKEY_print_params(stdout_bio, params,
				      5, NULL);
		
		printf("================= Public ================= \n");
		EVP_PKEY_print_public(stdout_bio, peer_key,
				      5, NULL);
	
		printf("================= Private ================= \n");
		EVP_PKEY_print_private(stdout_bio, peer_key,
				       5, NULL);
		
	}
#if 0	
	/* Get the peer's public key, and provide the peer with our public key -
	 * how this is done will be specific to your circumstances */
	peerkey = get_peerkey(pkey);
#endif
	/* Create the context for the shared secret derivation */
	if(NULL == (ctx = EVP_PKEY_CTX_new(pkey, NULL))) handleErrors();

	/* Initialise */
	if(1 != EVP_PKEY_derive_init(ctx)) handleErrors();

	/* Provide the peer public key */
	if(1 != EVP_PKEY_derive_set_peer(ctx, peer_key)) handleErrors();

	/* Determine buffer length for shared secret */
	if(1 != EVP_PKEY_derive(ctx, NULL, secret_len)) handleErrors();

	/* Create the buffer */
	if(NULL == (secret = OPENSSL_malloc(*secret_len))) handleErrors();

	/* Derive the shared secret */
	if(1 != (EVP_PKEY_derive(ctx, secret, secret_len))) handleErrors();

	EVP_PKEY_CTX_free(ctx);
	EVP_PKEY_free(peer_key);
	EVP_PKEY_free(pkey);
	EVP_PKEY_CTX_free(kctx);
	EVP_PKEY_free(params);
	EVP_PKEY_CTX_free(pctx);

	/* Never use a derived secret directly. Typically it is passed
	 * through some hash function to produce a key */
	return secret;

}

int main(int argc, char *argv[])
{
	size_t secret_len = 256+ 128;
	unsigned char * secret;

	srand(time(NULL));

	secret = ecdh(&secret_len);
	if (secret)
		hexprint(secret, secret_len);
		
	return 0;
}
