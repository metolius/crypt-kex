#!/bin/bash

# this function is called when Ctrl-C is sent
function trap_ctrlc ()
{
    # perform cleanup here
    echo "Ctrl-C caught...performing clean up"
 
    echo "Doing cleanup"
 
    # exit shell script with error code 2
    # if omitted, shell script will continue execution
    exit 2
}
# initialise trap to call trap_ctrlc function
# when signal 2 (SIGINT) is received
trap "trap_ctrlc" 2


BINARY=diffie
#SOURCE=diffie.c
SOURCE=ecdh.c
CC=gcc
INOTIFY='inotify -e modify'

case "$OSTYPE" in
    linux*)   INOTIFY='inotify -e modify' ;;
    darwin*)
	INOTIFY='fswatch -1 --event=Updated'
	LDFLAGS="-R/usr/local/opt/openssl/lib -L/usr/local/opt/openssl/lib"
	CPPFLAGS="-I/usr/local/opt/openssl/include"
	;;
    *)        INOTIFY='inotify -e modify' ;;
esac

$CC $CFLAGS $CPPFLAGS -Wall -Werror -g $SOURCE -o $BINARY -lcrypto
[ $? -ne 0 ] || ./$BINARY

while $INOTIFY $SOURCE
do
$CC $CFLAGS $CPPFLAGS -Wall -Werror -g $SOURCE -o $BINARY -lcrypto
[ $? -ne 0 ] || ./$BINARY

done
